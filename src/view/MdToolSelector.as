package view
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	/**
	 * Управляет поведением тулбара. Сам селектор выключен за ненадобностю. 
	 * Включить можно, нажав на кнопку insert.
	 * 
	 * @author phanatos
	 * 
	 */
	public class MdToolSelector extends Mediator
	{
		public static const NAME:String = 'MdToolSelector';
		
		// состояния тулбара
		public static const S_UNKNOWN:int = 0;
		// режим создания сущностей
		public static const S_ENTITY:int = 1;
		// режим создания связей
		public static const S_RELATION:int = 2;
		
		// состояния тулбара
		private static const LB_NORMAL:String = 'normal';
		private static const LB_SELECT:String = 'select';
		private static const LB_DISABLED:String = 'disabled';

		// ссылка на медиатор диаграммы
		private var mdDiagram:MdDiagram;
		// сслыка на тулбар
		private var toolbar:MovieClip
		
		// внутренне состояние тулбара. Соответствует выбранному инструменту
		private var _state:int = S_UNKNOWN;
		
		public function get state():int
		{
			return _state;
		}
		
		public function set state(value:int):void
		{
			if (_state != value)
			{
				_state = value;
				updateState();
			}
		} 
		
		public function MdToolSelector(mediatorName:String=null, viewComponent:Object=null)
		{
			super(NAME, viewComponent);
		} 
		
		public function get screen():MovieClip
		{
			return viewComponent as MovieClip;
		}
		
		override public function onRegister():void
		{
			mdDiagram = MdDiagram.getInstance();
			toolbar = screen.toolbar;
			toolbar.visible = false;
			initButton(toolbar.btnEntity, S_ENTITY);
			initButton(toolbar.btnRelation, S_RELATION);
			screen.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, true);
			screen.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp, false, 0, true); 
			state = S_ENTITY;
		}
		
		override public function onRemove():void
		{
			deinitButton(toolbar.btnEntity);
			deinitButton(toolbar.btnRelation);
			screen.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false); 
			screen.stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp, false); 
			toolbar.parent.removeChild(toolbar);
			toolbar = null;
			mdDiagram = null
		} 
		
		private function onKeyUp(event:KeyboardEvent):void
		{
			if (event.keyCode == Keyboard.INSERT) toolbar.visible = !toolbar.visible; 
			if (!mdDiagram.isDragging)
			{
				if (!event.ctrlKey) 
				{
					state = S_ENTITY;
				}
			}
		}
		
		private function onKeyDown(event:KeyboardEvent):void
		{
			if (!mdDiagram.isDragging)
			{
				
				if (event.ctrlKey) 
				{
					state = S_RELATION;
				}
			}
		}
		
		private function initButton(mc:MovieClip, state:int):void
		{
			mc.addEventListener(MouseEvent.CLICK, onBtnClick, false, 0, true);
			mc.state = state;
		}
		
		private function deinitButton(mc:MovieClip):void
		{
			mc.removeEventListener(MouseEvent.CLICK, onBtnClick, false);
			mc.state = null;
		}

		private function onBtnClick(event:Event):void
		{
			var mc:MovieClip = event.currentTarget as MovieClip;
			if (mc)
			{
				state = mc.state;	
			}
		}
		
		private function updateState():void
		{
			switch(state)
			{
				case S_ENTITY:
					toolbar.btnEntity.gotoAndStop(LB_SELECT);
					toolbar.btnRelation.gotoAndStop(LB_NORMAL);
					break;
				case S_RELATION:
					toolbar.btnEntity.gotoAndStop(LB_NORMAL);
					toolbar.btnRelation.gotoAndStop(LB_SELECT);
					break;
				default:
					throw new Error('unknown state ' + state.toString());
					break;
				
			}
			sendNotification(DiagramEvents.NOTE_TOOLBAR_STATE_CHANGED, state);
		}
		
	}
}
package model
{
	import flash.geom.Point;
	
	import utils.AdvMath;
	import utils.SerializeObject;
	
	/**
	 * Сущность. Представлена на диаграме прямоугольником 
	 * @author phanatos
	 * 
	 */
	public class Entity extends SerializeObject
	{
		/// встроенные цвета
		public static const C_UNKNOWN:int = 0;
		public static const C_BROWN:int = 1;
		public static const C_OLIVE:int = 2;
		public static const C_LIME:int = 3;
		public static const C_DARKCYAN:int = 4;
		public static const C_BLUE:int = 5;
		public static const C_PURPLE:int = 6;
		public static const C_PINK:int = 7;
		public static const C_RED:int = 8;
		
		/// массив доступных цветов
		public const COLORS:Array = [C_BROWN,C_OLIVE, C_LIME, C_DARKCYAN, C_BLUE, C_PURPLE, C_PINK, C_RED];
		/// массив доступных имен для сущностей
		public const NAMES:Array = ['fames', 'est', 'optimus', 'magister'];
		
		/// для создания уникальный ид
		private static var seq:int = 1;
		/// для наименования
		private static var idx:int = 0;
		
		/// позиция сущности
		public var pos:Point;
		/// цвет
		public var color:int = C_UNKNOWN;
		/// наименование
		public var caption:String = '';
		
		public function Entity()
		{
			super();
			id = seq ++;
			color = COLORS[AdvMath.randInt(0, COLORS.length - 1)];
			caption = NAMES[idx++];
			if (idx >= NAMES.length) idx = 0;
			
		}
		
		public function free():void
		{
		}
		
		
	
	}
}
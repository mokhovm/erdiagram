package main
{
	/**
	 * Описывает текущий режим работы программы. При релизном билде перезаписывается
	 * из шаблона, поэтому если что-то менять, то править надо template\Config.as    
	 * @author Phanatos
	 *  
	 */
	public class Config   
	{
		// текущий режим работы программы
		public static const CUR_MODE:int = DebugModes.MODE_DEBUG_LOCAL;
		//public static const CUR_MODE:int = DebugModes.MODE_RELEASE;
		//public static const CUR_MODE:int = DebugModes.MODE_DEBUG;
		//public static const CUR_MODE:int = DebugModes.MODE_DEBUG_LOCAL;
		public static const VERSION:String = '0.0.58';
	}   
}
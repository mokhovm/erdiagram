package model
{
	import utils.SerializeObject;
	
	/**
	 * Связь. Описывает соединение двух сущностей 
	 * @author phanatos
	 * 
	 */
	public class Relation extends SerializeObject
	{
		/// для создания уникальный ид
		private static var seq:int = 1;
		
		public var entity1:Entity;
		public var entity2:Entity;
		
		public function Relation(one:Entity, two:Entity)
		{
			super();
			id = seq ++;
			entity1 = one;
			entity2 = two;
		}
		
		/**
		 * Вернет тру, если эта связь связывает две этих сущности 
		 * @param one
		 * @param two
		 * @return 
		 * 
		 */
		public function isSame(one:Entity, two:Entity):Boolean
		{
			var res:Boolean = ((entity1 != null && entity2 != null) &&
				(entity1 == one && entity2 == two) || (entity2 == one && entity1 == two));
			return res;
		}
		
		/**
		 * Вернет тру, если эта связь связана с этой сущностью 
		 * @param entity
		 * @return 
		 * 
		 */
		public function connectToEntity(entity:Entity):Boolean
		{
			var res:Boolean = (entity1 == entity || entity2 == entity);
			return res;
		}
		
		public function free():void
		{
			entity1 = null;
			entity2 = null;
		}
	}
}
package main
{
	import controller.CmdAddRelation;
	import controller.CmdDeleteRelation;
	import controller.CmdAddEntity;
	import controller.CmdDeleteEntity;
	
	import flash.net.registerClassAlias;
	
	import model.PrAppModel;
	
	import org.puremvc.as3.patterns.facade.Facade;
	
	import view.MdDiagram;
	
	/**
	 * Фасад приложения. Регистрирует прокси, медиаторы и команды диаграммы. 
	 * @author phanatos
	 * 
	 */
	public class AppFacade extends Facade
	{
		public static const NAME:String = 'AppFacade';
		// Главный спрайт приложения
		public var screen:ERDiagram;
		
		// команды
		// добавить сущность
		public static const CMD_ADD_ENTITY:String = NAME + '/cmd/addEntity';
		// удалить сущность
		public static const CMD_DELETE_ENTITY:String = NAME + '/cmd/deleteEntity';
		// добавить отношение
		public static const CMD_ADD_RELATION:String = NAME + '/cmd/addRelation';
		// удалить отношение
		public static const CMD_DELETE_RELATION:String = NAME + '/cmd/deleteRelation';
		
		public function AppFacade()
		{
			super();
		}
		
		/**
		 * Стартап приложения. 
		 * Инициализируем части приложения 
		 * @param application
		 * 
		 */
		public function startup(application:ERDiagram):void
		{
			screen = application;
			registerTypes();
			registerProxy(new PrAppModel());
			registerMediator(new MdDiagram(null, screen));
		}
		
		public function shutdown():void
		{
			removeProxy(PrAppModel.MODEL_NAME);
			removeMediator(MdDiagram.NAME);
			screen = null;
		}
		
		/**
		 * Здесь регистрируются все необходимые для работы приложения классы 
		 * 
		 */
		private function registerTypes():void
		{
			registerClassAlias("ClpWrongFrame", ClpWrongFrame);
		}
		
		/**
		 * Регистрация команд приложения 
		 * 
		 */
		override protected function initializeController():void
		{
			super.initializeController();
			registerCommand(CMD_ADD_ENTITY, CmdAddEntity);
			registerCommand(CMD_DELETE_ENTITY, CmdDeleteEntity);
			registerCommand(CMD_ADD_RELATION, CmdAddRelation);
			registerCommand(CMD_DELETE_RELATION, CmdDeleteRelation);
		}
		
		/**
		 * Перекроем метод предка, чтобы получить удобную типизацию 
		 * @return 
		 * 
		 */
		public static function getInstance():AppFacade
		{
			if (instance == null)
			{
				instance = new AppFacade();
			}
			return instance as AppFacade;
		}
	}
}
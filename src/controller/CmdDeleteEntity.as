package controller
{
	
	import main.AppFacade;
	
	import model.Entity;
	import model.PrAppModel;
	import model.Relation;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	import view.DiagramEvents;
	import view.EntityModule;
	
	/**
	 * Удаляет некоторую сущность. Телом Entity
	 * @author phanatos
	 * 
	 */
	public class CmdDeleteEntity extends SimpleCommand
	{
		override public function execute(note:INotification):void
		{
			var appModel:PrAppModel = PrAppModel.getInstance();
			var entity:Entity = note.getBody() as Entity;
			if (entity)
			{
				// прежде чем удалить сущность, нужно удалить все её связи
				var list:Vector.<Relation> = appModel.getEntityRelations(entity);
				for (var i:int = 0; i < list.length; i++) 
				{
					var vo:RelationVO = new RelationVO();
					vo.relation = list[i];
					sendNotification(AppFacade.CMD_DELETE_RELATION, vo);
				}
				sendNotification(DiagramEvents.NOTE_ENTITY_DELETED, entity);
				appModel.deleteEntity(entity);
			}
		}
	}
}
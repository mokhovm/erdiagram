package view
{
	import controller.EntityVO;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.Mouse;
	
	import main.AppFacade;
	
	import model.Entity;
	import model.Relation;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	import utils.DisplayUtils;
	
	/**
	 * Отображает главный вид приложения, подключает тулбар и модули управления элементами диаграммы
	 * @author phanatos
	 * 
	 */
	public class MdDiagram extends Mediator
	{
		public static const NAME:String = 'MdDiagram';
		/// ширина и высота клипа для сущности
		public var clipWidth:int = 0;
		public var clipHeight:int = 0;
		/// осуществляется переноска
		public var isDragging:Boolean = false;
		/// текущий инструмент
		public var curTool:int = MdToolSelector.S_UNKNOWN;
		/// список клипов сущностей на экране
		public var boxes:Vector.<MovieClip>;
		/// список линков на экране
		public var links:Vector.<Sprite>;
		
		private var _scene:MovieClip; 
		
		public function MdDiagram(mediatorName:String=null, viewComponent:Object=null)
		{
			super(NAME, viewComponent); 
			_scene = new ClpScene(); 
			_scene.x = 0;
			_scene.y = 0;
			_scene.mouseEnabled = false;
			_scene.bkg.cacheAsBitmap = true;
			_scene.bkg.mouseChildren = false;
			_scene.bkg.mouseEnabled = false;
			(viewComponent as Sprite).addChild(_scene); 
		}
		
		public function get screen():MovieClip
		{
			return _scene;
		}
		
		override public function onRegister():void
		{
			boxes = new Vector.<MovieClip>;
			links = new Vector.<Sprite>;
			
			screen.stage.doubleClickEnabled = true;
			
			facade.registerMediator(new EntityModule(null, screen.layerEntity));
			facade.registerMediator(new RelationModule(null, screen.layerRelation));
			facade.registerMediator(new MdToolSelector(null, screen));

			screen.stage.addEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick, false, 0, true);	
			screen.stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove, false, 0, true);
			screen.stage.addEventListener(Event.MOUSE_LEAVE, onMouseLeave, false, 0, true);
		
			initEntitySize();
		}
		
		override public function onRemove():void
		{
			facade.removeMediator(EntityModule.NAME);
			facade.removeMediator(RelationModule.NAME);
			facade.removeMediator(MdToolSelector.NAME);
			
			screen.stage.removeEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick, false);
			screen.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove, false);
			screen.stage.removeEventListener(Event.MOUSE_LEAVE, onMouseLeave, false);
			
			boxes.length = 0;
			links.length = 0;
		}
		
		private function onMouseLeave(event:Event):void
		{
			sendNotification(DiagramEvents.NOTE_FOCUS_LOST);
		}
		
		private function onMouseMove(event:Event):void
		{
			if (isDragging)	
			{
				switch(curTool)
				{
					case MdToolSelector.S_ENTITY:
						sendNotification(DiagramEvents.NOTE_ENTITY_MOVED);
						break;
					case MdToolSelector.S_RELATION:
						sendNotification(DiagramEvents.NOTE_RELATION_BEGIN);
						break;
					default:
						throw new Error('unknown tool ' + curTool.toString());
						break;
				}
			}
		}
		
		private function onDoubleClick(event:Event):void
		{
			var vo:EntityVO = new EntityVO(screen.mouseX, screen.mouseY);
			sendNotification(AppFacade.CMD_ADD_ENTITY, vo);
		}
		
		public static function getInstance():MdDiagram
		{
			return AppFacade.getInstance().retrieveMediator(NAME) as MdDiagram;
		}
		
		private function changeTool(toolId:int):void
		{
			curTool = toolId;
		}		
		
		/**
		 * Найдет для сущности отображающий её клип 
		 * @param entity
		 * @return 
		 * 
		 */
		public function findClip(entity:Entity):MovieClip
		{
			var res:MovieClip;
			for (var i:int = 0; i < boxes.length; i++) 
			{
				if (boxes[i].entity == entity) 
				{
					res = boxes[i];
					break;
				}
			}
			return res;
			
		}
		
		/**
		 * Найдет на диаграмме линию, отображающую данное отношение 
		 * @param relation
		 * @return 
		 * 
		 */
		public function findLink(relation:Relation):Sprite
		{
			var res:Sprite;
			for (var i:int = 0; i < links.length; i++) 
			{
				if (links[i].metaData == relation)
				{
					res = links[i];
					break;
				}
			}
			return res;
		}
		
		/**
		 * кэширует текущий размер клипов сущностей 
		 * 
		 */
		private function initEntitySize():void
		{
			if (clipHeight == 0) 
			{
				var mc:MovieClip = new ClpEntity();
				clipWidth = mc.width;
				clipHeight = mc.height;
				mc = null;
			}
			
		}
		
		/**
		 * Ищет коробку под курсором
		 * Не очень юзабельно, поищем другим способом 
		 * @return 
		 * 
		public function getBoxUnderCursor():MovieClip
		{
			var res:MovieClip
			var list:Array = screen.getObjectsUnderPoint(new Point(screen.mouseX, screen.mouseY));
			for (var i:int = 0; i < list.length; i++) 
			{
				// тут хитро. getObjectsUnderPoint возвращает не сам клип, а его составляющие
				if (list[i].parent)
				{
					var mc:MovieClip = list[i].parent as MovieClip;
					if (mc && mc.entity)
					{
						res = mc;
						break;
					}
				}
			}
			return res;
		}
		*/
		
		/**
		 * Ищет коробку под курсором 
		 * @return 
		 * 
		 */
		public function getBoxUnderCursor():MovieClip
		{
			var res:MovieClip
			var mc:MovieClip;
			var rect:Rectangle;
			for (var j:int = 0; j < boxes.length; j++) 
			{	
				mc = boxes[j];
				rect = mc.getBounds(screen);
				if (rect.containsPoint(new Point(screen.mouseX, screen.mouseY)))
				{
					res = mc;
					break;
				}
			}
			return res;
			
		}
		
		
		/**
		 *  Слушаем следующие нотификации 
		 */		
		override public function listNotificationInterests():Array
		{
			
			return[ 
				DiagramEvents.NOTE_TOOLBAR_STATE_CHANGED
			];
		}
		
		/**
		 * Обрабатываем интересующие нас события 
		 * @param note - стандартное послание
		 * 
		 */		
		override public function handleNotification(note:INotification):void
		{
			super.handleNotification(note);
			
			switch (note.getName()) 
			{
				case DiagramEvents.NOTE_TOOLBAR_STATE_CHANGED:
					changeTool(note.getBody() as int);
					break;
				
			}
		}
		

	}
}
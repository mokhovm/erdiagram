package main 
{
	import com.demonsters.debugger.MonsterDebugger;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	import flash.events.KeyboardEvent;
	import flash.system.Security;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	
	[SWF(width="800", height="600", backgroundColor="#333333", frameRate="25")]
	/**
	 * Тестовое задание для Lesta Studio. В задании представлена условная реализация диаграммы
	 * Сущность-связь. Для запуска используется bin-debug/ERDiagram.html 
	 * 
	 * Сущности создаются на экране двойным щелчком мыши. Их можно перемещать
	 * по экрану с помощью мыши и удалять с экрана кликом по крестику. 
	 * Связи между сущностями создаются левой кнопкой мыши при нажатой клавише Ctrl. Повторный щелчек 
	 * по связи с нажатым контролом уничтожает связь.
	 * 
	 * Для реализации использована концепция MVC и её практическая реализация в библиотеке PureMVC
	 * Модель системы реализована в модуле model/PrAppModel, представление view/MdDiagram. 
	 * Контроллеры команд расположены в пакете controller. Также в проекте использованы ряд сервисных модулей, 
	 * которые находятся в каталоге Utils и добавляют некоторое удобство в написании кода. 
	 * Библиотека символов находится в файле view/layout/layout.fla. Она компилируется в swc, который 
	 * при сборке линкуется к основному приложению. Соответственно, классы, определенные в swc-библиотеке 
	 * доступны в основном домене приложения и автоматически подхватываются редактором. 
	 * Визуализация диаграммы построена по модульному принципу. Основной медиатор диаграммы MdDiagram формирует
	 * рабочее пространсто и подргужает разегистрированные модули и тулбар для переключения управления между ними.
	 * EntityModule визуализирует сущности на диаграмме и предоставляет интерфейс к командам управления сущностями. 
	 * RelationModule рисует связи между сущностями и предоставляет интерфейс к командам управления связями.
	 * Новый модуль для диаграммы должен наследоваться от CustomDiagramModule.
	 * 
	 * Дебаг версия скомпилирована с опцией -advanced-telemetry для отладки в AdobeScout   
	 * Для подключения MonsterDebugger надо раскомментировать метод initDebugger()
	 * 
	 * Модель приложения покрыта тестами. Для запуска следует использовать src/bin-debug/FlexUnitApplication.html
	 * (но не FlexUnitApplication.swf, так как в этом случае не поддерживается атрибут очередности запуска тестов)
	 * Подробная информация о тестах показывается в IDE FlashBuilder на вкладке FlexUnit Result
	 *    
	 * Для компиляции исходного кода в Flashdevelop необходимо...
	 * 
	 * @author Мохов Михаил (phanatos)
	 * 
	 */
	public class ERDiagram extends Sprite
	{
		public const APP_NAME:String = "ER Diagram. Ver: ";
		// ссылка на фасад
		public var facade:AppFacade;
		
		public function ERDiagram()
		{
			Security.allowDomain('*');
			facade = AppFacade.getInstance();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage, false, 0, true);
		}
		
		private function onAddedToStage(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false);
			// инициализируем подсистемы приложения
			initDebugger();
			initSystem();
			initScreen();
			initMenu();
			initKeyboard();
			initMouse();
			initDataFromLoader();
			facade.startup(this);
		}
		
		private function onRemoveFromStage(event: Event): void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage, false);
			contextMenu = null;
			facade.shutdown();
		}
		
		/**
		 * Для подключения внешнего дебаггера достаточно разкомментировать этот метод 
		 * 
		 */
		private function initDebugger():void
		{
			/*
			MonsterDebugger.initialize(this);
			MonsterDebugger.trace("main", "Debugger started");
			*/
		}
		
		private function initSystem():void
		{
			// может пригодится для управления GC в нагруженных приложениях
			//System.pauseForGCIfCollectionImminent(0.1);
		}
		
		private function initScreen():void
		{
			stage.tabChildren = false;
			//stage.addEventListener(FullScreenEvent.FULL_SCREEN, onFullScreen, false, 0, true);
		}
		private function initKeyboard():void
		{
			//stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, true);	
		}
		
		private function initMouse():void
		{
			Mouse.cursor = MouseCursor.AUTO;
		}
		
		private function initMenu():void
		{
			var myMenu:ContextMenu = new ContextMenu(); 
			var myItem:ContextMenuItem = new ContextMenuItem(APP_NAME + Config.VERSION); 
			myMenu.customItems.push(myItem); 
			contextMenu = myMenu; 
			contextMenu.builtInItems.forwardAndBack = false;
			contextMenu.builtInItems.loop = false;
			contextMenu.builtInItems.rewind = false;
			contextMenu.builtInItems.zoom = false;
			contextMenu.builtInItems.print = false;
		}
		
		private function initDataFromLoader():void
		{
		}
		
		private function onFullScreen(event: FullScreenEvent): void
		{
		}
		
		private function onKeyDown(event:KeyboardEvent):void
		{
		}
	}
}
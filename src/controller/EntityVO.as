package controller
{
	import flash.geom.Point;
	
	import model.Entity;
	
	import utils.SerializeObject;
	
	/**
	 * Вспомогательный объект для команды CMD_ADD_ENTITY
	 * @author phanatos
	 * 
	 */
	public class EntityVO extends SerializeObject
	{
		public var pos:Point;
		public var entity:Entity;
		
		public function EntityVO(x:int, y:int)
		{
			super();
			pos = new Point(x, y);
		}
	}
}
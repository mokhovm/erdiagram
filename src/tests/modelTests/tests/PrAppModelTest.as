package tests.modelTests.tests
{
	import flexunit.framework.Assert;
	
	import model.Entity;
	import model.PrAppModel;
	import model.Relation;

	/**
	 * Тест модели приложения 
	 * @author phanatos
	 * 
	 */
	public class PrAppModelTest
	{		
		
		private var appModel:PrAppModel;
		
		[Before]
		public function setUp():void
		{
			appModel = new PrAppModel();
		}
		
		[After]
		public function tearDown():void
		{
			appModel = null;
		}
		
		[BeforeClass]
		public static function setUpBeforeClass():void
		{
		}
		
		[AfterClass]
		public static function tearDownAfterClass():void
		{
		}
		
		[Test( description = "Initialisation success" )]
		public function testConstructor():void
		{
			Assert.assertEquals('правильная установка имени прокси', appModel.getProxyName(), PrAppModel.MODEL_NAME);
			Assert.assertNotNull('список сущностей создан', appModel.entities);
			Assert.assertNotNull('список связей создан', appModel.relations);
		}
		
		[Test( description = "test addEntity method" )]
		public function testAddEntity():void
		{
			var e1:Entity;
			e1 = appModel.addEntity();
			Assert.assertEquals('Сущность успешно добавилась', appModel.entities.length, 1);
			Assert.assertObjectEquals('Добавилась та самая сущность', e1, appModel.entities[0]);
		}
		
		[Test( description = "test deleteEntity method" )]
		public function testDeleteEntity():void
		{
			var e1:Entity = appModel.addEntity();
			var e2:Entity = appModel.addEntity();
			appModel.deleteEntity(e1);
			Assert.assertEquals('Сущность успешно удалилась', appModel.entities.length, 1);
			Assert.assertObjectEquals('Осталась нужная сущность', appModel.entities[0], e2);
		}
		
		[Test( description = "test addRelation method" )]
		public function testAddRelation():void
		{
			var e1:Entity = appModel.addEntity();
			var e2:Entity = appModel.addEntity();
			var r:Relation = appModel.addRelation(e1, e2);
			Assert.assertEquals('Связь успешно добавилась', appModel.relations.length, 1);
			Assert.assertObjectEquals('Добавилась та самая связь', r, appModel.relations[0]);
			r = appModel.addRelation(e1, e1);
			Assert.assertNull('Нельзя соединить одну и ту же сущность', r);
			
		}
		
		
		
		[Test( description = "test deleteRelation method" )]
		public function testDeleteRelation():void
		{
			var e1:Entity = appModel.addEntity();
			var e2:Entity = appModel.addEntity();
			var e3:Entity = appModel.addEntity();
			var e4:Entity = appModel.addEntity();
			var r1:Relation = appModel.addRelation(e1, e2);
			var r2:Relation = appModel.addRelation(e3, e4);
			appModel.deleteRelation(r1);
			Assert.assertEquals('Связь успешно удалилась', appModel.relations.length, 1);
			Assert.assertNull('Ссылка на сущность1 удалена', r1.entity1);
			Assert.assertNull('Ссылка на сущность2 удалена', r1.entity2);
			Assert.assertObjectEquals('Осталась нужная связь', appModel.relations[0], r2);
			
		}
		
		[Test( description = "test hasRelation method" )]
		public function testHasRelation():void
		{
			var e1:Entity = appModel.addEntity();
			var e2:Entity = appModel.addEntity();
			var e3:Entity = appModel.addEntity();
			var r1:Relation = appModel.addRelation(e1, e2);
			Assert.assertTrue('Есть такая связь', appModel.hasRelation(e1, e2));
			Assert.assertFalse('Нет такой связи1', appModel.hasRelation(e1, e3));
			Assert.assertFalse('Нет такой связи2', appModel.hasRelation(e3, e1));
		}

		[Test( description = "test getRelation method" )]
		public function testGetRelation():void
		{
			var e1:Entity = appModel.addEntity();
			var e2:Entity = appModel.addEntity();
			var e3:Entity = appModel.addEntity();
			var e4:Entity = appModel.addEntity();
			var r1:Relation = appModel.addRelation(e1, e2);
			var r2:Relation = appModel.addRelation(e3, e4);
			Assert.assertObjectEquals('Ту ли связь получили1', appModel.getRelation(e1, e2), r1);
			Assert.assertTrue('Ту ли связь получили2', appModel.getRelation(e1, e2) != r2);
		}
		
		[Test( description = "test getEntityRelations method" )]
		public function testGetEntityRelations():void
		{
			var e1:Entity = appModel.addEntity();
			var e2:Entity = appModel.addEntity();
			var e3:Entity = appModel.addEntity();
			var e4:Entity = appModel.addEntity();
			
			var list:Vector.<Relation>;
			list = appModel.getEntityRelations(e1);
			Assert.assertEquals('Список связей должен быть пуст 1', list.length, 0);
			
			
			var r1:Relation = appModel.addRelation(e1, e2);
			var r2:Relation = appModel.addRelation(e1, e3);
			var r3:Relation = appModel.addRelation(e2, e3);
			
			list = appModel.getEntityRelations(e4);
			Assert.assertEquals('Список связей должен быть пуст 2', list.length, 0);
			 
			list = appModel.getEntityRelations(e1);
			Assert.assertEquals('Список связей для e1(1)', list.length, 2);
			
			appModel.deleteRelation(r1);
			
			list = appModel.getEntityRelations(e1);
			Assert.assertEquals('Список связей для e1(2)', list.length, 1);
		}
		






		
		
	}
}
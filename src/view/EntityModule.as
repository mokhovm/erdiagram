package view
{
	import controller.RelationVO;
	
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.sampler.stopSampling;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	
	import main.AppFacade;
	
	import model.Entity;
	import model.PrAppModel;
	import model.Relation;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	import utils.DisplayUtils;
	import utils.PlayOnce;
	import controller.CheckPlaceVO;
	
	/**
	 * Модуль для отображения сущностей на диаграмме. 
	 * Умеет создавать перемещать и удалять специальные объекты - боксы, представляющие на 
	 * диаграмме сущности предметной области. 
	 * Бокс реализован с помощью специального клипа, созданного в layout.fla
	 * Связь между сущностью и боксом осуществляется через динамическое поле box.entity
	 * @author phanatos
	 * 
	 */
	public class EntityModule extends CustomDiagramModule
	{
		public static const NAME:String = 'EntityModule';

		// начальные координаты клипа при претаскивании
		private var oldX:Number;
		private var oldY:Number;
	  
		// клип сущности, который переносят
		private var draggingClip:MovieClip;
		// если тру, то курсор находится над боксом 
		private var isOverbox:Boolean;
		
		// объекты для подсветки сущностей при наведении курсора
		private var glowFilter:GlowFilter;
		private var filters:Array = [];
		
		private const FILTER_COLOR:int = 0x99FF00;
		private const FILTER_STRENGTH:Number = 1.5;
		
		
		public function EntityModule(mediatorName:String=null, viewComponent:Object=null)
		{
			super(NAME, viewComponent);
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			DisplayUtils.clearContainer(screen);
			moduleTool = MdToolSelector.S_ENTITY;
			glowFilter = new GlowFilter();
			glowFilter.color = FILTER_COLOR; 
			glowFilter.strength = FILTER_STRENGTH;
			filters.push(glowFilter);
		}
		
		override public function onRemove():void
		{
			deleteBoxes();
			glowFilter = null;
			filters.length = 0;
			super.onRemove();
		}
		
		/**
		 * Удаляет все боксы 
		 * 
		 */
		private function deleteBoxes():void
		{
			while (mdDiagram.boxes.length > 0)
			{
				deleteConcreteBox(mdDiagram.boxes[0]);		
			}
		}	
		
		/**
		 * Пытается удалить бокс, соответствующий определенной сущности 
		 * @param entity
		 * 
		 */
		private function tryDeleteBox(entity:Entity):void
		{
			if (entity)
			{
				var mc:MovieClip = mdDiagram.findClip(entity);
				if (mc) deleteConcreteBox(mc);
			}
		}	
		
		/**
		 * Удаляет конкретный бокс 
		 * @param mc
		 * 
		 */
		private function deleteConcreteBox(box:MovieClip):void
		{
			DisplayUtils.removeFromScreen(box);
			box.removeEventListener(MouseEvent.MOUSE_DOWN, onBoxDown, false);
			box.removeEventListener(MouseEvent.MOUSE_UP, onBoxUp, false);
			box.removeEventListener(MouseEvent.MOUSE_OVER, onBoxOver, false);
			box.removeEventListener(MouseEvent.MOUSE_OUT, onBoxOut, false);
			box.btnClose.removeEventListener(MouseEvent.CLICK, onCloseClick, false);
			var index:int = mdDiagram.boxes.indexOf(box);
			mdDiagram.boxes.splice(index, 1);
			isOverbox = false;
			updateCursor();
			trace('delete box. Count ' + mdDiagram.boxes.length);
		}
		
		/**
		 * Обновляет позицию бокса 
		 * @param box
		 * 
		 */
		private function updateBoxPosition(box:MovieClip):void
		{
			//sendNotification(NOTE_ENTITY_MOVED, box.entity); 
			var list:Vector.<Relation> = appModel.getEntityRelations(box.entity);
			for (var i:int = 0; i < list.length; i++) 
			{
				sendNotification(DiagramEvents.NOTE_RELATION_INVALIDATE, list[i]);
			}
		}	
		
		/**
		 * Добавляет бокс, представляющий некоторую сущность, на диаграмму 
		 * @param entity
		 * 
		 */
		private function addBox(entity:Entity):void
		{
			if (entity)  
			{
				var box:ClpEntity2 = new ClpEntity2();
				screen.addChild(box);
				box.addEventListener(MouseEvent.MOUSE_DOWN, onBoxDown, false, 0, true);
				box.addEventListener(MouseEvent.MOUSE_UP, onBoxUp, false, 0, true);
				box.addEventListener(MouseEvent.MOUSE_OVER, onBoxOver, false, 0, true);
				box.addEventListener(MouseEvent.MOUSE_OUT, onBoxOut, false, 0, true);
				box.btnClose.addEventListener(MouseEvent.CLICK, onCloseClick, false, 0, true);
				box.btnClose.visible = false;
				box.lbName.text = entity.caption;
				box.x = entity.pos.x;
				box.y = entity.pos.y;
				box.entity = entity;
				box.gotoAndStop(entity.color);
				box.cacheAsBitmap = true;
				mdDiagram.boxes.push(box);
				trace('add box. Count ' + mdDiagram.boxes.length);
			}
		}
		
		
		/**
		 * Начинает перетаскивание бокса. 
		 * @param event
		 * 
		 */
		private function onBoxDown(event:Event):void
		{
			var mc:MovieClip = (event.currentTarget as MovieClip);
			if (mc)
			{
				if (!mdDiagram.isDragging && isUsed())
				{
					mdDiagram.isDragging = true;
					startDragEntity(mc);			
				}
			}
		}
		
		/**
		 * Заканчивает перетаскивание бокса 
		 * @param event
		 * 
		 */
		private function onBoxUp(event:Event):void
		{
			var mc:MovieClip = (event.currentTarget as MovieClip);
			if (mc)
			{
				if (mdDiagram.isDragging && draggingClip && isUsed())
				{ 
					stopDragEntity(draggingClip);
				}	
			}
		}	
		
		private function onBoxOver(event:Event):void
		{
			var box:MovieClip = (event.currentTarget as MovieClip);
			if (box)
			{
				box.filters = filters;
				box.btnClose.visible = true;
				isOverbox = true;
				updateCursor();
			}
		}
		
		private function onBoxOut(event:Event):void
		{
			var box:MovieClip = (event.currentTarget as MovieClip);
			if (box)
			{
				box.filters = null;
				isOverbox = false;
				box.btnClose.visible = false;
				updateCursor();
			}
		}
		
		override protected function updateCursor():void
		{
			if (isOverbox)
			{
				if (isUsed())
					Mouse.cursor = MouseCursor.HAND;
				else
					Mouse.cursor = MouseCursor.BUTTON;
			}
			else
			{
				Mouse.cursor = MouseCursor.AUTO;
			}
		}
		
		/**
		 * Обрабочик кнопки закрытия для бокса  
		 * @param event
		 * 
		 */
		private function onCloseClick(event:Event):void
		{
			var box:MovieClip = (event.currentTarget as SimpleButton).parent as MovieClip;
			if (box)
			{
				sendNotification(AppFacade.CMD_DELETE_ENTITY, box.entity);
				event.stopPropagation();
			}
		}	
		
		/**
		 * Проверяет, есть ли местеко, чтобы положить или создать сущность по указанным координатам  
		 * @param vo
		 * 
		 */
		private function checkFreePlace(vo:CheckPlaceVO):void
		{
			if (vo)
			{
				vo.canCreate = true;
				
				// надо найти, нет ли на экране в пределах прямоугольника, образованного 
				// координатами клика и размервами стандартного клипа других таких клипов
				for (var i:int = 0; i < mdDiagram.boxes.length; i++) 
				{
					var mc:MovieClip = mdDiagram.boxes[i];
					if (vo.excludeEntity && mc.entity == vo.excludeEntity) continue;
					var newRect:Rectangle = new Rectangle(vo.pos.x, vo.pos.y, mdDiagram.clipWidth, mdDiagram.clipHeight);
					var rect:Rectangle = new Rectangle(mc.x, mc.y, mc.width, mc.height);
					if (rect.intersects(newRect))
					{
						vo.canCreate = false;
						break;
					}
				}
			}
		}
		
		/**
		 * Показывает пояснение игроку, что клип сущности здесь не может быть расположен 
		 * @param point
		 * 
		 */
		private function showWrongFrame(point:Point):void
		{
			if (point)
			{
				var mc:MovieClip;
				mc = PlayOnce.play('ClpWrongFrame', screen);
				mc.x = point.x;
				mc.y = point.y;
			}
		}
		
		
		/**
		 * Начинает драг бокса 
		 * @param mc
		 * 
		 */
		private function startDragEntity(box:MovieClip):void
		{
			oldX = box.x;
			oldY = box.y;
			box.parent.setChildIndex(box, box.parent.numChildren - 1); 
			box.startDrag(false, new Rectangle(0, 0, box.stage.stageWidth - box.width, 
				box.stage.stageHeight - box.height));
			draggingClip = box;
		}
		
		/**
		 * Клип перенесен, заканчиваем обратоку
		 * @param mc
		 * 
		 */
		private function stopDragEntity(mc:MovieClip):void
		{
			mdDiagram.isDragging = false;
			updateBoxPosition(mc);
			mc.stopDrag();
			draggingClip = null;
			var vo:CheckPlaceVO = new CheckPlaceVO(mc.x, mc.y);
			vo.excludeEntity = mc.entity;
			checkFreePlace(vo);
			if (!vo.canCreate)
			{
				mc.x = oldX;
				mc.y = oldY;
				updateBoxPosition(mc);
			}
			
			
		}	
		
		/**
		 *  Слушаем нотификации 
		 */		
		override public function listNotificationInterests():Array
		{
			var events:Array = super.listNotificationInterests();
			events.push(
				DiagramEvents.NOTE_ENTITY_ADDED,
				DiagramEvents.NOTE_CHECK_PLACE,
				DiagramEvents.NOTE_WRONG_FRAME,
				DiagramEvents.NOTE_ENTITY_DELETED,
				DiagramEvents.NOTE_ENTITY_MOVED,
				DiagramEvents.NOTE_FOCUS_LOST
			);
			return events; 
		}
		
		/**
		 * Обрабатываем интересующие нас события 
		 * @param note - стандартное послание
		 * 
		 */		
		override public function handleNotification(note:INotification):void
		{
			super.handleNotification(note);
			
			switch (note.getName()) 
			{
				case DiagramEvents.NOTE_ENTITY_ADDED:
					addBox(note.getBody() as Entity);
					break;
				case DiagramEvents.NOTE_CHECK_PLACE:
					checkFreePlace(note.getBody() as CheckPlaceVO);
					break;
				case DiagramEvents.NOTE_WRONG_FRAME:
					showWrongFrame(note.getBody() as Point);
					break;
				case DiagramEvents.NOTE_ENTITY_DELETED:
					tryDeleteBox(note.getBody() as Entity);
					break;
				case DiagramEvents.NOTE_ENTITY_MOVED:
					if (draggingClip && draggingClip.entity)
						updateBoxPosition(draggingClip);
					break;
				case DiagramEvents.NOTE_FOCUS_LOST:
					if (mdDiagram.isDragging && draggingClip && isUsed())
						stopDragEntity(draggingClip);
					break;
			}
		}
		
		
	}
}
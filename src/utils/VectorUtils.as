package utils
{
	
	import flash.utils.ByteArray;
	
	/**
	 * Здесь лежат всякие утилиты для работы с нашим типом данных Vector.<uint>
	 * @author Phanatos
	 * 
	 */	
	public class VectorUtils extends Object
	{
		public function VectorUtils()
		{
			super();
		}
		
		
		/**
		 * Быстрый поиск в массиве ИД (массив должен быть отсортирован!) 
		 * @param keys - массив id
		 * @param key - значение элемента
		 * @return - вернет индекс этого элемента в массиве
		 * 
		 */
		public static function binarySearch(keys:Vector.<int>, key:int):int 
		{
			var res:int = -1;
			
			if (keys != null && keys.length != 0)
			{
				var min:int = 0;
				var max:int = keys.length - 1;
				// continue searching while [imin,imax] is not empty
				while (max >= min)
				{
					// calculate the midpoint for roughly equal partition
					var mid:int = int((max + min ) / 2);
					if (keys[mid] == key)
					{
						// key found at index imid
						res = mid;
						break;
					}
					else if (keys[mid] < key)
						// change min index to search upper subarray
						min = mid + 1;
					else         
						// change max index to search lower subarray
						max = mid - 1;
				}
				
			}
			return res;
		}
		
		
		/**
		 * копирует содержимое одного вектора в другой 
		 * @param source - откуда
		 * @param dest - куда
		 */		
		public static function copyVector(source:Vector.<uint>, dest:Vector.<uint>):void
		{
			for each (var id:uint in source)
			dest.push(id);		
			dest.sort(compare);
		}
		
		/**
		 * Сортирует вектор по id 
		 * @param source
		 * 
		 */		
		public static function sortVector(source:Vector.<uint>):void
		{
			source.sort(compare);	
		}
		
		/**
		 * очищает вектор 
		 * @param source
		 * 
		 */		
		public static function clearVector(source:Vector.<uint>):void
		{
			source.splice(0,source.length);
		}
		
		/**
		 * Дополняет dest значениями из source до размера size, если это возможно, при условии, что  
		 * все значения в dest будут уникальными.  
		 * @param source
		 * @param dest
		 * @param size - максимальный размер dest. Если dest уже имеет такой размер, функция тут же завершается 
		 */		
		public static function completeVector(source:Vector.<uint>,dest:Vector.<uint>, size:uint):void
		{
			var add:Vector.<uint>;
			//source = source.sort(compare);
			//dest = dest.sort(compare);
			if (dest.length >= size) return;
			for (var i:uint = 0; i < source.length; i++)
			{
				if (dest.indexOf(source[i]) == -1) 
				{
					dest.push(source[i]);
					if (dest.length >= size) {
						break;	
					}
				}
			}
			dest.sort(compare);
		}
		
		
		private static function compare(x:uint, y:uint):Number 
		{
			if (x < y) return -1
			else if (x == y) return 0
			else return 1
		}
		
		/**
		 * Перетасовывает массив или вектор в случайном порядке 
		 * @param obj
		 * 
		 */
		public static function shuffleObject(obj:Object):void
		{
			var i:int = obj.length;
			while (i > 0) 
			{
				var j:int = Math.floor(Math.random() * i);
				i--;
				var temp:* = obj[i];
				obj[i] = obj[j];
				obj[j] = temp;
			}				
		}
		
		
		/**
		 * Прверяет массив данных на уникальность и вернет только уникальные значения 
		 * @param ids - массив
		 * @return массив с уникальными значениями 
		 * 
		 */
		public static function getUniqueValues(ids:Array):Array
		{
			var unic:Array = new Array();
			var existValues:Object = new Object();
			var i:int;
			while (i < ids.length) {
				if ((((typeof(existValues[ids[i]]) == "undefined")) || ((existValues[ids[i]] == null)))){
					unic.push(ids[i]);
				};
				existValues[ids[i]] = ids[i];
				i++;
			};
			return (unic);
		}
		
		
	}
	
}
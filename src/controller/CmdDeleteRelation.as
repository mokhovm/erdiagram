package controller
{
	import model.PrAppModel;
	import model.Relation;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	import view.DiagramEvents;
	import view.EntityModule;
	
	/**
	 * Удаляет связь. Телом - экземпляр RelationVO 
	 * @author phanatos
	 * 
	 */
	public class CmdDeleteRelation extends SimpleCommand
	{
		override public function execute(note:INotification):void
		{
			var appModel:PrAppModel = PrAppModel.getInstance();
			
			var vo:RelationVO = note.getBody() as RelationVO;
			if (vo)
			{
				var aRelation:Relation = (vo.relation != null ? vo.relation : appModel.getRelation(vo.one, vo.two));
				sendNotification(DiagramEvents.NOTE_RELATION_DELETED, aRelation);
				appModel.deleteRelation(aRelation);
			}
		}
	}
}
package tests.modelTests
{
	import tests.modelTests.tests.EntityTest;
	import tests.modelTests.tests.PrAppModelTest;
	import tests.modelTests.tests.RelationTest;

	[Suite]
	[RunWith("org.flexunit.runners.Suite")]
	public class ModelTests
	{
		public var entityTest:EntityTest;   
		public var relationTest:RelationTest;  
		public var modelTest:PrAppModelTest;
	}
}
package utils
{
	import flash.net.getClassByAlias;
	import flash.system.ApplicationDomain;
	import flash.utils.ByteArray;

	/**
	 * Вспосогательные утилиты для работы с классами 
	 * @author phanatos
	 * 
	 */
	public class ClassUtils
	{
		
		/**
		 * Зарегистрирован ли в текущем домене приложения такой класс 
		 * @param className
		 * @return 
		 * 
		 */
		public static function hasClass(className:String):Boolean
		{
			return ( ApplicationDomain.currentDomain.hasDefinition(className)); 	
		}
		
		
		/**
		 * Ищет класс по имени 
		 * @param className
		 * @return вернет класс или null
		 * 
		 */
		public static function getClass(className:String):Class
		{
			var res:Class;
			if (ApplicationDomain.currentDomain.hasDefinition(className))
				res = ApplicationDomain.currentDomain.getDefinition(className) as Class;
			else
			{
				try
				{
					res = getClassByAlias(className);	
				} 
				catch(error:Error) 
				{
					//Cc.error('className ' + className + ' is not found with message:' + error.message);					
				}
			}
			//if (res == null) Cc.warn('Could not find className: ' + className);
			return res;
		}
		
		/**
		 * Клонирует объект 
		 * @param object
		 * @return 
		 * 
		 */
		public static function clone(object:Object):Object 
		{
			var a:Object;
			var copier:ByteArray = new ByteArray();
			copier.writeObject(object);
			copier.position = 0;
			a = copier.readObject();
			return(a);
		}
		
	}		
}
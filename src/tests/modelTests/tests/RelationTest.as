package tests.modelTests.tests
{
	import flexunit.framework.Assert;
	
	import model.Entity;
	import model.Relation;

	/**
	 * Тест класса Relation 
	 * @author phanatos
	 * 
	 */
	public class RelationTest
	{		
		public var relation:Relation;
		public var e1:Entity;
		public var e2:Entity;
		
		[Before]
		public function setUp():void
		{
			e1 = new Entity();
			e2 = new Entity();
			relation = new Relation(e1, e2);
		}
		
		[After]
		public function tearDown():void
		{
			relation.free();
			relation = null;
			e1 = null;
			e2 = null;
		}
		
		[BeforeClass]
		public static function setUpBeforeClass():void
		{
		}
		
		[AfterClass]
		public static function tearDownAfterClass():void
		{
		}
		
		[Test( description = "Initialisation success" )]
		[Rule(order=1)]
		public function testConstructor():void
		{
			Assert.assertObjectEquals('after construction entity1 will be set', relation.entity1, e1);
			Assert.assertObjectEquals('after construction entity2 will be set', relation.entity2, e2);
			Assert.assertEquals('check id select for relation:',relation.id, 1);
		}
		
		[Test( description = "Deinitialisation success" )]
		[Rule(order=2)]
		public function testDestructor():void
		{
			relation.free();
			Assert.assertObjectEquals('after destructor entity1 will be null', relation.entity1, null);
			Assert.assertObjectEquals('after destructor entity2 will be null', relation.entity2, null);
		}
		
		[Test( description = "Checking isSame method" )]
		[Rule(order=3)]
		public function testIsSame():void
		{
			var e3:Entity = new Entity();
			Assert.assertTrue('check of the same entities', relation.isSame(e1, e2));
			Assert.assertTrue('check of entities in reverse order', relation.isSame(e2, e1));
			Assert.assertFalse('check of another entities', relation.isSame(e2, e3));
			Assert.assertFalse('check of another entities 2', relation.isSame(e3, e2));
		}
		
		[Test( description = "Checking connectToEntity method" )]
		[Rule(order=4)]
		public function testConnectToEntity():void
		{
			var e3:Entity = new Entity();
			Assert.assertTrue('the entity connected with this relation', relation.connectToEntity(e1));
			Assert.assertTrue('the entity connected with this relation', relation.connectToEntity(e2));
			Assert.assertFalse('the entity NOT connected with this relation', relation.connectToEntity(e3));
		}
		
		
	}
}
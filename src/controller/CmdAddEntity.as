package controller
{
	import model.PrAppModel;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	import view.DiagramEvents;
	import view.EntityModule;
	
	/**
	 * Добавляет новую сущность. Телом EntityVO
	 * @author phanatos
	 * 
	 */
	public class CmdAddEntity extends SimpleCommand
	{
		override public function execute(note:INotification):void
		{
			var appModel:PrAppModel = PrAppModel.getInstance();
			var vo:EntityVO = note.getBody() as EntityVO;
			
			if (canCreate(vo.pos.x, vo.pos.y))
			{
				vo.entity = appModel.addEntity();
				vo.entity.pos = vo.pos;
				sendNotification(DiagramEvents.NOTE_ENTITY_ADDED, vo.entity);
			}
			else
				sendNotification(DiagramEvents.NOTE_WRONG_FRAME, vo.pos);
		}
		
		/**
		 * Проверяет, можно ли создать сущность.
		 * Для этого надо определить, есть ли для неё место. Разошлем событие,
		 * чтобы заинтересованные медиаторы сделали это для нас
		 * @return 
		 * 
		 */
		private function canCreate(x:int, y:int):Boolean
		{
			var res:Boolean;
			var vo:CheckPlaceVO = new CheckPlaceVO(x, y);
			sendNotification(DiagramEvents.NOTE_CHECK_PLACE, vo);
			res = vo.canCreate;
			return res;
		}
	}
}
package utils
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	public class DisplayUtils
	{
		
		/**
		 * очищает содержимое контейнера 
		 * @param obj
		 * 
		 */		
		public static function clearContainer(obj:DisplayObjectContainer):void
		{
			var child:DisplayObject;
			if (obj is MovieClip) (obj as MovieClip).stop();
			while (obj.numChildren > 0)
			{
				child = obj.getChildAt(0);
				child = null;
				obj.removeChildAt(0);
			}
		}
		
		
		/**
		 * очищает содержимое контейнера, но не меняет его геометрический размер, добавляя 
		 * невидимый прямоугольный такого же размера.  
		 * @param obj
		 * 
		 */
		public static function suspendContainer(obj:DisplayObjectContainer):void
		{
			var r:Rectangle = obj.getBounds(obj);
			DisplayUtils.clearContainer(obj);
			var sprite:Sprite = new Sprite();
			sprite.graphics.beginFill(0, 0);
			sprite.graphics.drawRect(r.left, r.top, r.width, r.height);
			sprite.graphics.endFill();
			obj.addChild(sprite);
		}		
		
		/**
		 * Создает изображение по названию класса и помещает в контейнер 
		 * @param imgClassName - название класса изображения
		 * @param owner - контейнер для визуализации
		 * @param needClear - очищать ли контейнер перед помещением картинки
		 * @return - возвращает экземпляр созданного изображения 
		 */		
		public static function createImage(imgClassName:String, owner:DisplayObjectContainer):Bitmap
		{
			
			var classRef:Class;
			var img:BitmapData;
			var bm:Bitmap;
			
			if (imgClassName != '')
			{
				classRef = ClassUtils.getClass(imgClassName) as Class;
				img = new classRef() as BitmapData;
				bm = new Bitmap(img);
				owner.addChild(bm);
			}
			return bm;
		}
		
		/**
		 * Создает клип по названию класса и помещает в контейнер. 
		 * @param clpClassName - название класса клипа
		 * @param owner - контейнер для визуализации
		 * @return - возвращает экземпляр созданного клипа 
		 * 
		 */		
		public static function createClip(clpClassName:String, 
			owner:DisplayObjectContainer, needToClear:Boolean = false):MovieClip
		{
			
			var classRef:Class;
			var img:BitmapData;
			var bm:Bitmap;   
			var mc:MovieClip;
			
			if (needToClear)
			{
				clearContainer(owner);
			}
			
			if (clpClassName != '')
			{ 
				classRef = ClassUtils.getClass(clpClassName) as Class;
				mc = new classRef() as MovieClip; 
				if (owner != null) owner.addChild(mc); 
				mc.x = 0;
				mc.y = 0;
			} 
			return mc; 
		} 	
		
		/**
		 * Центрует объект obj относительно своего владельца
		 * Точка регистрации obj должна находится в 0,0, иначе будет работать неправильно.
		 * Пока проблему не решил   
		 * @param obj
		 * 
		 */		
		public static function centerObj(obj:DisplayObject):void
		{
			if (obj.parent != null)
			{
				/*
				var theRect:Rectangle = obj.getRect(obj.parent);
				var centerX:Number = theRect.x+(theRect.right-theRect.left)/2;
				var centerY:Number = theRect.y+(theRect.bottom-theRect.top)/2;
				*/
				obj.x = (obj.parent.width / 2) - (obj.width / 2);
				obj.y = (obj.parent.height / 2) - (obj.height / 2);
			}
			
			
		}
		
		/**
		 * Возвращает корневого родителя экранного объекта
		 * @param obj - экранный объект
		 * @return - корневой родителя или null, если предка у объекта нет
		 * 
		 */		
		public static function getRootParent(obj:DisplayObject):DisplayObjectContainer
		{
			var res:DisplayObjectContainer;
			
			res = obj.parent;
			if (res != null)
			{
				while (true)
				{
					if (res.parent != null)
						res = res.parent;
					else
						break;
				}
			}
			return res;
		}
		
		
		/**
		 * возвращает номер кадра по метке (как оказалось не нужно, так как gotoAndPlay
		 * понимает и метки тоже)
		 * @param mc
		 * @param frameLabel
		 * @return 
		 * 
		 */
		public static function getFrameByLabel(mc: MovieClip, labelName: String ):int
		{
			var frameNumber:int = -1;
			
			for( var k:int = 0 ; k < mc.scenes.length ; k++ )
				for( var i:int = 0 ; i < mc.scenes[k].labels.length ; i++ )
				{
					if( mc.scenes[k].labels[i].name == labelName )
						frameNumber = mc.scenes[k].labels[i].frame;
				}
			
			return frameNumber;
		}
		
		/**
		 * Есть ли в данном клипе такая метка 
		 * @param mc
		 * @param labelName
		 * @return 
		 * 
		 */
		public static function hasFrameLabel(mc: MovieClip, labelName: String ):Boolean
		{
			var res:Boolean;
			
			var labels:Array = mc.currentLabels;
			for (var i:int = 0; i < labels.length; i++) 
			{
				if (labels[i] == labelName)
				{
					res = true;
					break;
				}
			}
			return res;
		}
		
		public static function getGlobalCoord(mc:MovieClip):Point
		{
			// собственные координаты мувиклипа учитываться не должны!
			//var p:Point = new Point(mc.x, mc.y);
			var p:Point = new Point(0, 0);
			p = mc.localToGlobal(p);
			return p;
		}
		
		
		/**
		 * Останавливает мувик и все вложенные в него мувики 
		 * @param field
		 * @param obj
		 * @return 
		 * 
		 */
		public static function stopMovie(mc:MovieClip):void
		{
			if (mc != null)
			{
				mc.stop();
				for (var i:int = 0; i < mc.numChildren; i++) 
				{
					stopMovie(mc.getChildAt(i) as MovieClip);
				}
			}
		}
		
		
		/**
		 * Перебирает детишек obj в поисках инстанции field 
		 * @param field
		 * @param obj
		 * @return 
		 * 
		 */
		public static function getInstanceInDisplayObject(field:String, obj:MovieClip):DisplayObject
		{
			var res:DisplayObject;
			if (obj != null)
			{
				if (obj.hasOwnProperty(field)) 
				{
					res = obj[field];
				}
				else 
				{
					for (var i:int = 0; i < obj.numChildren; i++) 
					{
						res = getInstanceInDisplayObject(field, obj.getChildAt(i) as MovieClip);
						if (res != null) break;
					}
				}
			}
			return res;
		}
		
		
		/**
		 * Ищет в объекте и его детях инстанцию с указанным именем 
		 * @param name
		 * @param obj
		 * @return 
		 * 
		 */
		public static function findDisplayObjectByName(name:String, obj:MovieClip):DisplayObject
		{
			var res:DisplayObject;
			if (obj != null)
			{
				if (obj.name == name) 
				{
					res = obj;
				}
				else 
				{
					for (var i:int = 0; i < obj.numChildren; i++) 
					{
						res = findDisplayObjectByName(name, obj.getChildAt(i) as MovieClip);
						if (res != null) break;
					}
				}
			}
			return res;
		}
		
		/**
		 * Удаляет объект с экрана  
		 * @param obj
		 * 
		 */
		public static function removeFromScreen(obj:DisplayObject):void
		{
			if (obj != null && obj.parent != null)
			{
				obj.parent.removeChild(obj);
			}
		}
		
		/**
		 * Сортирует объекты на экране по оси Y 
		 * @param container
		 * 
		 */
		public static function sortChildrenByY(container:DisplayObjectContainer):void 
		{
			var i:int;
			var childList:Array = new Array();
			i = container.numChildren;
			while(i--)
			{
				childList[i] = container.getChildAt(i);
			}
			childList.sortOn("y", Array.NUMERIC);
			i = container.numChildren;
			while(i--)
			{
				if (childList[i] != container.getChildAt(i))
				{
					container.setChildIndex(childList[i], i);
				}
			}
		}
		
		/**
		 * Выдает лоадер, который подгружает картинку с указанного урл 
		 * @param url
		 * @return 
		 * 
		 */
		public static function getImg(url:String):Loader
		{
			var res:Loader = new Loader();
			if (url != null && url != '') res.load(new URLRequest(url));
			//loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImgLoad)
			res.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onLoadError, false, 0, true);
			return (res);
		}
		
		public static function onLoadError(event:Event):void
		{
			trace('img load error');
		}
		
		/**
		 * Проверяет, есть ли мышка над объектом 
		 * @param d
		 * @return 
		 * 
		 */
		public static function detectMouseOver(d:DisplayObject):Boolean
		{
			var res:Boolean;
			var mousePoint:Point = d.localToGlobal(new Point(d.mouseX, d.mouseY));
			res = d.hitTestPoint(mousePoint.x, mousePoint.y,true);
			return res;
		}
		
		/**
		 * Вернет тру, если ролик работает в полноэкранном режиме 
		 * @param stage
		 * @return 
		 * 
		 */
		public static function isFullScreen(stage:Stage):Boolean
		{
			return (stage.displayState != StageDisplayState.NORMAL); 
		}
		
		/**
		 * Меняет точку регистраци контейнера 
		 * @param obj
		 * @param newX
		 * @param newY
		 * 
		 */
		public static function setRegPoint(obj:DisplayObjectContainer, newX:Number, newY:Number):void 
		{
			//get the bounds of the object and the location 
			//of the current registration point in relation
			//to the upper left corner of the graphical content
			//note: this is a PSEUDO currentRegX and currentRegY, as the 
			//registration point of a display object is ALWAYS (0, 0):
			var bounds:Rectangle = obj.getBounds(obj.parent);
			var currentRegX:Number = obj.x - bounds.left;
			var currentRegY:Number = obj.y - bounds.top;
			
			var xOffset:Number = newX - currentRegX;
			var yOffset:Number = newY - currentRegY;
			//shift the object to its new location--
			//this will put it back in the same position
			//where it started (that is, VISUALLY anyway):
			obj.x += xOffset;
			obj.y += yOffset;
			
			//shift all the children the same amount,
			//but in the opposite direction
			for(var i:int = 0; i < obj.numChildren; i++) {
				obj.getChildAt(i).x -= xOffset;
				obj.getChildAt(i).y -= yOffset;
			}
		}
		
		/**
		 * Растягивает объект относительно указанной точки 
		 * @param objToScale
		 * @param regX
		 * @param regY
		 * @param scaleX
		 * @param scaleY
		 * 
		 */
		public static function scaleAroundPoint(objToScale:DisplayObject, regX:int, regY:int, 
			scaleX:Number, scaleY:Number):void
		{
			if (!objToScale){
				return;
			}
			var transformedVector:Point = new Point( (regX-objToScale.x)*scaleX, (regY-objToScale.y)*scaleY );
			
			objToScale.x = regX-( transformedVector.x);
			objToScale.y = regY-( transformedVector.y);
			objToScale.scaleX =  objToScale.scaleX*(scaleX);
			objToScale.scaleY =  objToScale.scaleY*(scaleY);
		}
		
		
		/**
		 * Делает клип прозрачным для мыши 
		 * @param clip
		 * 
		 */
		public static function mouseOff(mc:MovieClip):void
		{
			if (mc != null)
			{
				mc.mouseChildren = false;
				mc.mouseEnabled = false;
			}
		}		
		
		/**
		 * Растеризует кадр клипа и возвращает его в виде спрайта. 
		 * @param mc - клип для ратеризации.
		 * @param frameLabel - метка кадра клипа.
		 * @param matrix - это если надо как-то это окно сдвинуть или исказить. Вот тут наглядное описание
		 * матричных преобразований:
		 * http://www.tomauger.com/2013/flash-actionscript/actionscript-3-bitmapdata-draw-offset-and-positioning
		 * http://www.senocular.com/flash/tutorials/transformmatrix/
		 * @param rect - размеры окна, которое надо растеризировать (используются только width и height).
		 * если окно надо сдвинуть, то используй матрицу 
		 * @return 
		 * 
		 */
		public static function cacheFrame(mc:MovieClip, rect:Rectangle, frameLabel:String = '', 
			matrix:Matrix = null):Sprite
		{
			var res:Sprite = new Sprite();
			var lastFrame:int = 0;
			
			var myBitmapData:BitmapData = new BitmapData(rect.width, rect.height, true, 0x000000);
			var myBitmap:Bitmap = new Bitmap(myBitmapData);
			if (frameLabel != '')
			{
				lastFrame = mc.currentFrame;
				mc.gotoAndStop(frameLabel);
			}
			myBitmapData.draw(mc, matrix, null, null, rect, false);
			
			if (lastFrame != 0) mc.gotoAndStop(lastFrame);
			
			res.addChild(myBitmap);
			
			return res;
		}
		
		
		
	}
}
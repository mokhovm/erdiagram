package controller
{
	import model.Entity;
	import model.Relation;
	
	import utils.SerializeObject;
	 
	/**
	 * Вспомогательный объект для команды CmdAddRelation
	 * @author phanatos
	 *  
	 */  
	public class RelationVO extends SerializeObject
	{
		public var one:Entity;
		public var two:Entity;
		public var relation:Relation;
		
		public function RelationVO(entity1:Entity = null, entity2:Entity = null)
		{
			super();
			one = entity1;
			two = entity2;
		}
	}
}
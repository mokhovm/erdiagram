package utils
{
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.events.Event;

	/**
	 * Проигрывает клип до конца и уничтожает его
	 * пользоваться так: PlayOnce.play('clipClass', parent); 
	 * 
	 * !!!По умолчанию отключает интерактивность клипа (вызывает DisplayUtls.mouseOff(mc);)
	 * 
	 * @author phanatos
	 * 
	 */
	public class PlayOnce
	{
		public static var instance:PlayOnce;
		private var list:Vector.<MovieClip>;
		
		public function PlayOnce()
		{
			list = new Vector.<MovieClip>;
		}
		
		/**
		 * Добавить клип к проигрывателю 
		 * @param className - название клипа
		 * @param owner - родитель, куда автоматически помещается клип
		 * @return ссылка на созданный клип
		 * 
		 */
		protected function addClip(className:String, owner:DisplayObjectContainer = null):MovieClip
		{
			var mc:MovieClip;
			
			mc = DisplayUtils.createClip(className, owner);
			if (mc != null)
			{
				DisplayUtils.mouseOff(mc);
				mc.addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);	
				list.push(mc);
			}
			return mc;
		}

		
		/**
		 * Обработка кадров 
		 * @param event
		 * 
		 */
		private function onEnterFrame(event:Event):void
		{
			var mc:MovieClip = event.currentTarget as MovieClip;
			
			if (mc.onLabel != null && mc.currentFrameLabel != null) 
			{
				mc.onLabel(mc, mc.currentFrameLabel);
			}
			if (mc.currentFrame == mc.totalFrames)
			{
				if (mc.onComplete != null) 
				{
					mc.onComplete(mc);
					mc.onComplete = null;
					mc.onLabel = null;
				}
				releaseClip(mc);  
			}
		}
		
		/**
		 * Освобождает клип и ресурсы 
		 * @param mc
		 * 
		 */
		private function releaseClip(mc:MovieClip):void
		{
			if (mc != null)
			{
				mc.stop();
				mc.removeEventListener(Event.ENTER_FRAME, onEnterFrame, false);
				if (mc.parent != null) mc.parent.removeChild(mc);
				var index:int = list.indexOf(mc);
				list.splice(index, 1);
			}
		}
		
		protected static function getInstance():PlayOnce
		{
			if (PlayOnce.instance == null) PlayOnce.instance = new PlayOnce();
			return PlayOnce.instance;
		}

		
		/**
		 *  
		 * @param className - название клипа
		 * @param owner - где нарисовать
		 * @param aOnComplete - если надо получить сигнал об окончании проигрывнания клипа
		 * 						function onComplete(mc:MovieClip):void
		 * @param aOnLabel - если надо получить сигнал о метке
		 * 						function onLabel(mc:MovieClip, label:String):void
		 * @return 
		 * 
		 */
		public static function play(className:String, owner:DisplayObjectContainer = null, 
			aOnComplete:Function = null, aOnLabel:Function = null):MovieClip
		{
			var res:MovieClip;
			if (className != null)
			{
				var instance:PlayOnce = getInstance();
				res = instance.addClip(className, owner);
				if (res != null)
				{
					res.onComplete = aOnComplete;
					res.onLabel = aOnLabel;
				}
			}
			return res;
		}
	}
}
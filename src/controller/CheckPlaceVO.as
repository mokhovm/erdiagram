package controller
{
	import flash.geom.Point;
	
	import model.Entity;
	
	import utils.SerializeObject;
	
	/**
	 * Используется как параметр для нотификации NOTE_CHECK_PLACE 
	 * @author phanatos
	 * 
	 */
	public class CheckPlaceVO extends SerializeObject
	{
		public var pos:Point
		public var canCreate:Boolean;
		public var excludeEntity:Entity;
		
		public function CheckPlaceVO(x:int, y:int)
		{
			super();
			pos = new Point(x,y);
		}
	}
}
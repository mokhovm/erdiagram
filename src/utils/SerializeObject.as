package utils
{
	import flash.net.registerClassAlias;
	import flash.utils.ByteArray;
	
	/**
	 * Этот класс имеет методы для сериализации и десериализации JSON 
	 * Внимание, модуль компилируется только под Flash Player 11, AIR 3.0 и выше,
	 * так как использует встроенный JSON
	 * @author Phanatos
	 * 
	 */	
	public class SerializeObject
	{
		// станадртный идентификатор
		//public var id:uint = 0;
		public var id:int = 0;
		
		public function SerializeObject()
		{
			registerClassAlias("SerializeObject", SerializeObject);
		}
		
		/**
		 * Сериализует себя в строку. Но есть один эпик фейл. Этот сериализатор не умеет сериализовать 
		 * вектора! =) 
		 * @return 
		 * 
		 */
		public function serialize():String
		{
			return JSON.stringify(this);
		}
		
		/**
		 * Строку превращает в объект 
		 * @param s
		 * @return 
		 * 
		 */
		public function deserialize(s:String):Object
		{
			return JSON.parse(s);
		}
		
		
		/**
		 *  Инициализирует этот экземпляр данными из объекта
		 *  Инициализируются только простые свойства. Остальные надо устнавливать руками в потомках
		 */		
		public function initialize(data:Object):void
		{
			for (var field:* in data)
			{
				if (this.hasOwnProperty(field))
				{
					var s:String = typeof(this[field]);
					if ( s == 'string' || s == 'number' || s == 'boolean')
					{
						this[field] = data[field];
					}
				}
			}
			
		}
		
		/**
		 * Попытка инициализировать массивы автоматом. К векторам всё-равно не подходит 
		 * @param dataArray
		 * @param localArray
		 * @param itemType
		 * 
		 */
		public function deserializeArray(dataArray:Object, localArray:Array, itemType:Class):void
		{
			var item:SerializeObject;
			for each (var obj:Object in dataArray) 
			{
				item = new itemType();
				item.initialize(obj);
				localArray.push(item);
			}
			
		}
		
		/**
		 * Создает копию объекта
		 * @return - возвращает динамический объект, который следует привести к нужному типу
		 * Для клонирования нужно, чтобы все классы, входящие в данынй объект были зарегистированы
		 * функцией registerClassAlias(). 
		 * !!!возможны проблемы с векторами, поэтому клонировать лучше только простые VO объекты 
		 * без векторов
		 * 
		 */		
		public function clone():* {
			var a:*;
			var copier:ByteArray = new ByteArray();
			copier.writeObject(this);
			copier.position = 0;
			a = copier.readObject();
			return(a);
		}
		
		/**
		 * Клонирование объектов через JSON. Так как у нас почти все классы умеют десериализоваться
		 * через JSON, то это простой и доступный метод клонирования. Но опять есть одна досадная проблема:
		 * JSON.encode не умеет сериализовать вектора.... Может быть в будущем исправят 
		 * @return 
		 * 
		 */
		public function copyFrom(obj:SerializeObject):void
		{
			var s:String = obj.serialize();
			var data:Object = JSON.parse(s);
			initialize(data);
		}		
		
		
	}
	
}
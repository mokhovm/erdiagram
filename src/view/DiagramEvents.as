package view
{
	/**
	 * Описываются общие нотификации для диаграммы.
	 * @author phanatos
	 * 
	 */
	public class DiagramEvents
	{
		// нотификация о том, что изменилось состояние тулбара. Телом - ид состояния.
		public static const NOTE_TOOLBAR_STATE_CHANGED:String = '/note/toolbarStateChanged';
		
		// проверяет, есть ли по данным координатам свободное место, чтобы создать новую сущность.
		// использует CheckPlaceVO
		public static const NOTE_CHECK_PLACE:String = '/note/checkPlace';
		// требует отобразить сообщение, что в этом месте сущность создать нельзя. Телом идет Point
		public static const NOTE_WRONG_FRAME:String = '/note/wrongFrame';
		// нотификация о том, что добавилась новая сущность. Телом добавленная Entity
		public static const NOTE_ENTITY_ADDED:String = '/note/entityAdded';
		// нотификация о том, что сущность удаляется. Телом удаляемая Entity
		public static const NOTE_ENTITY_DELETED:String = '/note/entityDeleted';
		// нотификация о том, что добавилась связь. Телом Relation.
		public static const NOTE_RELATION_ADDED:String = '/note/relationAdded';
		// нотификация о том, что связь удаляется. Телом Relation.
		public static const NOTE_RELATION_DELETED:String = '/note/relationDeleted';
		
		
		// сообщает, что сущность поменяла координаты. Телом ничего не посылается, так как речь идет 
		// о конкретной инстанции, на которую ссылается проперть draggedClip
		public static const NOTE_ENTITY_MOVED:String = '/note/entityMoved';
		// сообщает, что надо перерисовать связь. Телом Relation
		public static const NOTE_RELATION_INVALIDATE:String = '/note/relationInvalidate';
		// сообщает, что юзер начинает создавать связь. Тело пустое.
		public static const NOTE_RELATION_BEGIN:String = '/note/relationCreate';
		// если приложение теряет фокус. Тело пустое.
		public static const NOTE_FOCUS_LOST:String = '/note/focusLost';
	}
}
package utils
{
	/**
	 * Некоторые полезные мат. и алгоритмические функции 
	 * @author phanatos
	 * 
	 */
	public class AdvMath
	{
		
		
		/**
		 * Увеличивает или уменьшает значение Value на процент от этого значения 
		 * @param value
		 * @param precent
		 * @param max - максимальное значение параметра после изменения
		 * @return 
		 * 
		 */
		public static function modifyValue(value:Number, precent:Number, min:Number, max:Number):Number
		{
			value += value / 100 * precent;
			if (value > max) value = max;
			if (value < min) value = min;
			return value;
		}
		
		
		/**
		 * если значение выходит за границы, то оно будет модифицировано по этим границам 
		 * @param value
		 * @param min
		 * @param max
		 * @return 
		 * 
		 */
		public static function limitValue(value:Number, min:Number, max:Number):Number
		{
			if (value > max) value = max;
			if (value < min) value = min;
			return value;
			
		}		
		
		
		/**
		 * Возвращает -1, если value отрицательное и 1 - если положительное
		 * @param value
		 * @return 
		 * 
		 */
		public static function sign(value:Number):int
		{
			return (value < 0 ? -1 : 1);
		}
		
		
		/**
		 * радианы в градусы 
		 * @param radians
		 * @return 
		 * 
		 */
		public static function radToDeg(radians:Number):Number
		{
			return (radians * 180/Math.PI);
		}
		
		/**
		 * Градусы в радианы 
		 * @param degrees
		 * @return 
		 * 
		 */
		public static function degToRad(degrees:Number):Number
		{
			return (degrees * Math.PI/180);
		}
		
		
		/**
		 * сработала ли вероятность 
		 * @param prob
		 * @return 
		 * 
		 */
		public static function checkProb(prob:Number):Boolean
		{	
			var res:Boolean;  
			if (prob == 0) 
				res = false;
			else
				res = (Math.random() <= prob);
			return res;
		}
		
		/**
		 * Генерирует случайное число от до.   
		 * @param from
		 * @param to
		 * @return - возвращает число типа Number. Если вызывающей функции нужен int
		 * то нужно округлять результат, иначе последнего числа не будет 
		 * 
		 */
		public static function rand(from:Number, to:Number):Number
		{	
			var res:Number = (Math.random() * (to - from)) + from; 
			return res;
		}
		
		/**
		 * Генерирует целое случайное число от и до  
		 * @param from
		 * @param to
		 * @return 
		 * 
		 */
		public static function randInt(from:int, to:int):int
		{	
			var res:int = Math.round(rand(from, to)); 
			return res;
		}		
		
		/**
		 * генерирует случайный знак 
		 * @return 
		 * 
		 */
		public static function randSign():int
		{	
			var res:int;
			res = Math.round(Math.random());
			res = (res == 0) ? -1: 1;
			return res;
		}	
		
		/**
		 * выбирает из массива случайный объект.  
		 * @param array - массив непрерывный, не должен иметь разрывов
		 * @return 
		 * 
		 */
		public static function randFromArray(array:Array):Object
		{
			var res:Object
			var i:int = randInt(0, array.length - 1);
			res = array[i];
			return res;
		}
		
		/**
		 * выбирает из массива случайный объект.  
		 * @param array - массив непрерывный, не должен иметь разрывов
		 * @return 
		 * 
		 */
		public static function randFromIntArray(data:Vector.<int>):int
		{
			var res:int
			var i:int = randInt(0, data.length - 1);
			res = data[i];
			return res;
		}	
		
		
		/**
		 * Округляет до знака после запятой
		 * @param value - значение для округления
		 * @param precision - количество знаков после запятой
		 * @return - округленное значение
		 * 
		 */
		public static function roundTo (value:Number, precision:int = 0):Number 
		{
			var res:Number;
			var n:Number = Math.pow(10,  precision);
			res = Math.round(value * n)/n;
			return res;
		}
		
		
	}
}
package tests.modelTests.tests
{
	import flexunit.framework.Assert;
	
	import model.Entity;

	/**
	 * Тест класса Entity 
	 * @author phanatos
	 * 
	 */
	public class EntityTest
	{		
		private var entity:Entity; 
		
		[Before]
		public function setUp():void
		{
			entity = new Entity();
		}
		
		[After]
		public function tearDown():void
		{
			entity = null;
		}
		
		[BeforeClass]
		public static function setUpBeforeClass():void
		{
		}
		
		[AfterClass]
		public static function tearDownAfterClass():void
		{
		}
		
		[Test( description = "Initialisation success" )]
		[Rule(order=1)]
		public function testConstructor():void
		{
			Assert.assertEquals('проверяем увеличение секвенции ид', entity.id, 1);
			Assert.assertEquals('выдача нового имени', entity.caption, entity.NAMES[0]);
			Assert.assertTrue('выдача случайного цвета', entity.color != Entity.C_UNKNOWN);
		}
		
		[Test( description = "check cycle name set" )]
		[Rule(order=2)]
		public function testConstructor2():void
		{
			Assert.assertEquals('проверяем увеличение секвенции ид', entity.id, 2);
			Assert.assertEquals('выдача нового имени', entity.caption, entity.NAMES[1]);
			Assert.assertTrue('выдача случайного цвета', entity.color != Entity.C_UNKNOWN);
			
			// создадим такое количество сущностей, чтобы имена пошли по второму разу
			for (var i:int = 1; i < entity.NAMES.length; i++) 
			{
				var e1:Entity = new Entity();	
			}
			// и проверим корректность повтороного использвания имен 
			Assert.assertEquals('повторная выдача имени', e1.caption, e1.NAMES[0]);
		}

		
	}
}
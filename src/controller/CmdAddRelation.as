package controller
{
	import model.PrAppModel;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	import view.DiagramEvents;
	import view.EntityModule;
	
	/**
	 * Добавляет связь. Телом RelationVO
	 * @author phanatos
	 * 
	 */
	public class CmdAddRelation extends SimpleCommand
	{
		override public function execute(note:INotification):void
		{
			var appModel:PrAppModel = PrAppModel.getInstance();
			
			var vo:RelationVO = note.getBody() as RelationVO;
			if (vo)
			{
				vo.relation = appModel.addRelation(vo.one, vo.two);
				// связь может и не быть создана, если она уже существует
				if (vo.relation)
					sendNotification(DiagramEvents.NOTE_RELATION_ADDED, vo.relation);
			}
		}
	}
}
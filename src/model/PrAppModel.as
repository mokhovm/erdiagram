package model
{
	import main.AppFacade;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * Моделька предметной области 
	 * @author phanatos
	 * 
	 */
	public class PrAppModel extends Proxy
	{
		public static const MODEL_NAME:String = 'PrAppModel';
		/// список сущностей
		public var entities:Vector.<Entity>;
		/// список связей
		public var relations:Vector.<Relation>;
		
		public function PrAppModel(proxyName:String=null, data:Object=null)
		{
			super(MODEL_NAME, data);
			entities = new Vector.<Entity>;
			relations = new Vector.<Relation>;
		}
		
		
		public function addEntity():Entity
		{
			var res:Entity = new Entity();
			entities.push(res);
			trace('add entity. Сount:' + entities.length);
			return res;
		}
		
		
		public function deleteEntity(aEntity:Entity):void
		{
			if (aEntity)
			{
				var index:int = entities.indexOf(aEntity);
				entities.splice(index, 1);
				aEntity.free();
				trace('delete entity. Сount:' + entities.length );
			}
		}
		
		public function addRelation(one:Entity, two:Entity):Relation
		{
			var res:Relation;
			if (one != two && !hasRelation(one, two))
			{
				res = new Relation(one, two);
				relations.push(res);
				trace('create relation. Count:' + relations.length);
			}
			return res;
		}
		
		public function deleteRelation(aRelation:Relation):void
		{
			if (aRelation)
			{
				var index:int = relations.indexOf(aRelation);
				relations.splice(index, 1);
				aRelation.free();
				trace('delete relation. Count:' + relations.length);
			}
		}
		
		/**
		 * Проверяет, есть ли у текущей сущности связь с указаной сущностью 
		 * @param aEntity - указанная сущность
		 * @return вернет ссылку на связь или null
		 * 
		 */
		public function hasRelation(one:Entity, two:Entity):Boolean
		{
			var res:Boolean = getRelation(one, two) != null;
			return res;
		}
		
		
		/**
		 * Вернет связь, которая связывает эти две сущности или null, если такой связи нет 
		 * @param one
		 * @param two
		 * @return 
		 * 
		 */
		public function getRelation(one:Entity, two:Entity):Relation
		{
			var res:Relation;
			for (var i:int = 0; i < relations.length; i++) 
			{
				if (relations[i].isSame(one, two)) 
				{
					res = relations[i];
					break;
				}
			}
			return res;
		}
		
		/**
		 * Вернет список связей сущности 
		 * @param entity
		 * @return 
		 * 
		 */
		public function getEntityRelations(entity:Entity):Vector.<Relation>
		{
			var res:Vector.<Relation> = new Vector.<Relation>;
			for (var i:int = 0; i < relations.length; i++) 
			{
				if (relations[i].connectToEntity(entity)) 
				{
					res.push(relations[i]);
				}
			}
			return res;
		}
		
		
		public static function getInstance():PrAppModel
		{
			return AppFacade.getInstance().retrieveProxy(MODEL_NAME) as PrAppModel;
		}
		
		
		
	}
}
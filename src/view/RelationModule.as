package view
{
	import controller.RelationVO;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	
	import main.AppFacade;
	
	import model.PrAppModel;
	import model.Relation;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	import utils.DisplayUtils;
	 
	/**
	 * Модуль для отображения отношений на диаграмме.
	 * Отношения представлены в виде специальных объектов - линков. 
	 * Линки реализованы в виде спрайтов, на которых рисуются линии, связывающие боксы.
	 * Линк сохраняет ссылку на связь в поле link.metaData
	 * @author phanatos
	 * 
	 */
	public class RelationModule extends CustomDiagramModule
	{
		
		public static const NAME:String = 'RelationModule';
		
		/// Цвет линка
		private const LINK_COLOR:uint = 0x99FF00;
		/// размер линии
		private const LINK_SIZE:Number = 4;
		
		/// при создании связи этот клип преставляет собой первую сущность
		private var startBox:MovieClip;
		/// признак того, что курсор находится над объектом
		private var isOverLink:Boolean;
		
		public function RelationModule(mediatorName:String=null, viewComponent:Object=null)
		{
			super(NAME, viewComponent);
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			DisplayUtils.clearContainer(screen);
			moduleTool = MdToolSelector.S_RELATION;
			screen.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp, false, 0, true);
			screen.stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown, false, 0, true);
		}
		
		override public function onRemove():void
		{
			screen.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp, false);
			screen.stage.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown, false);
			clearLinks();
			super.onRemove();
		}
		
		private function clearLinks():void
		{
			while (mdDiagram.links.length > 0)
			{
				deleteConcreteLink(mdDiagram.links[0]);		
			}
		}	
		
		/**
		 * Удаляет представление связи на диаграмме
		 * @param relation
		 * 
		 */
		private function deleteLink(relation:Relation):void
		{
			if (relation)
			{
				var link:Sprite = mdDiagram.findLink(relation);
				if (link)
				{
					link.removeEventListener(MouseEvent.CLICK, onLinkMouseClick, false);
					link.removeEventListener(MouseEvent.MOUSE_OVER, onLinkMouseOver, false);
					link.removeEventListener(MouseEvent.MOUSE_OUT, onLinkMouseOut, false);
					deleteConcreteLink(link);
					trace('delete link. Count ' + mdDiagram.links.length);
				}
			}
		}
		
		private function deleteConcreteLink(link:Sprite):void
		{
			link.graphics.clear();
			DisplayUtils.removeFromScreen(link);
			var index:int = mdDiagram.links.indexOf(link);
			mdDiagram.links.splice(index, 1);
		}

		
		private function addLink(relation:Relation):void
		{
			if (relation)
			{
				var link:Sprite = createConcreteLink();
				link.addEventListener(MouseEvent.CLICK, onLinkMouseClick, false, 0, true);
				link.addEventListener(MouseEvent.MOUSE_OVER, onLinkMouseOver, false, 0, true);
				link.addEventListener(MouseEvent.MOUSE_OUT, onLinkMouseOut, false, 0, true);
				link.metaData = relation;
				screen.addChild(link);
				mdDiagram.links.push(link);
				updateLink(relation, link);
				trace('create link. Count ' + mdDiagram.links.length);
			}
		}
		
		private function createConcreteLink():Sprite
		{
			var res:Sprite = new Sprite();
			screen.addChild(res);
			return res;
		}
		
		/**
		 * Этот метод используется для прорисовки линка, который находится в процессе
		 * создания и не прикреплен к боксу. Для хранения этого линка используется динамическое 
		 * свойство link у бокса, от которого начинается будущая связь  
		 * 
		 */
		private function beginLink():void
		{
			if (startBox)
			{
				if (!startBox.link) startBox.link = createConcreteLink();
				paintLink(startBox.link, 
					new Point(startBox.x + startBox.width/2, startBox.y + startBox.height / 2),
					new Point(screen.mouseX, screen.mouseY));
			}
		}
		
		private function onMouseDown(event:Event):void
		{
			if (!mdDiagram.isDragging && isUsed())	
			{
				startBox = mdDiagram.getBoxUnderCursor();
				if (startBox) 
				{
					mdDiagram.isDragging = true;
					trace('start drag');
				}
			}
		}
		
		private function onMouseUp(event:Event):void
		{
			if (mdDiagram.isDragging && isUsed())	
			{
				mdDiagram.isDragging = false;
				var mc:MovieClip = mdDiagram.getBoxUnderCursor();
				if (startBox && mc)
				{
					var vo:RelationVO = new RelationVO(startBox.entity, mc.entity);
					sendNotification(AppFacade.CMD_ADD_RELATION, vo);
				}
				else
				{
					trace('reject');
				}
				if (startBox)
				{
					DisplayUtils.removeFromScreen(startBox.link);
					startBox.link = null;
					startBox = null;
				}
			}
		}

		
		/**
		 * При клике на линке данный линк удаляется   
		 * @param event
		 * 
		 */
		private function onLinkMouseClick(event:Event):void
		{
			if (isUsed())
			{
				var link:Sprite = event.currentTarget as Sprite;
				if (link)
				{
					var vo:RelationVO = new RelationVO();
					vo.relation = link.metaData as Relation;
					sendNotification(AppFacade.CMD_DELETE_RELATION, vo);
				}
			}
		}
		
		private function onLinkMouseOver(event:Event):void
		{
			if (isUsed())
			{
				isOverLink = true;
				updateCursor();
			}
		}
		
		private function onLinkMouseOut(event:Event):void
		{
			isOverLink = false;
			updateCursor();
		}
		
		/**
		 * Обновляет изображение линка в зависимости от состояния связи. 
		 * @param relation
		 * @param link
		 * 
		 */
		private function updateLink(relation:Relation, link:Sprite = null):void
		{
			if (relation)
			{
				var clip1:MovieClip = mdDiagram.findClip(relation.entity1);
				var clip2:MovieClip = mdDiagram.findClip(relation.entity2);
				if (clip1 && clip2)
				{
					var link:Sprite = (link != null ? link : mdDiagram.findLink(relation));
				
					if (link)
					{
						paintLink(link, new Point(clip1.x + clip1.width / 2, clip1.y + clip1.height / 2),
							new Point(clip2.x + clip2.width / 2, clip2.y + clip2.height / 2));
						//trace('update link');
					}
				}	
			}
		}
		
		/**
		 * Рисует линк на указанном спрайте, в соответствии с координатами 
		 * @param link
		 * @param point1
		 * @param point2
		 * 
		 */
		private function paintLink(link:Sprite, point1:Point, point2:Point):void
		{
			link.graphics.clear();
			link.graphics.lineStyle(LINK_SIZE, LINK_COLOR);
			//link.graphics.beginFill(LINK_COLOR);
			link.graphics.moveTo(point1.x, point1.y);
			link.graphics.lineTo(point2.x, point2.y);
			//link.graphics.endFill();
		}
		
		override protected function updateCursor():void
		{
			if (isOverLink)
			{
				if (mdDiagram.curTool == MdToolSelector.S_RELATION)
					Mouse.cursor = MouseCursor.BUTTON;
				else
					Mouse.cursor = MouseCursor.AUTO;
			}
			else
			{
				Mouse.cursor = MouseCursor.AUTO;
			}
		}
		
		/**
		 *  Слушаем нотификации 
		 */		
		override public function listNotificationInterests():Array
		{
			var events:Array = super.listNotificationInterests();
			events.push(
				DiagramEvents.NOTE_RELATION_ADDED,
				DiagramEvents.NOTE_RELATION_DELETED,
				DiagramEvents.NOTE_RELATION_INVALIDATE,
				DiagramEvents.NOTE_RELATION_BEGIN
			);
			return events; 
		}
		
		/**
		 * Обрабатываем интересующие нас события 
		 * @param note - стандартное послание
		 * 
		 */		
		override public function handleNotification(note:INotification):void
		{
			super.handleNotification(note);
			
			switch (note.getName()) 
			{
				case DiagramEvents.NOTE_RELATION_ADDED:
					addLink(note.getBody() as Relation);
					break;
				case DiagramEvents.NOTE_RELATION_DELETED:
					deleteLink(note.getBody() as Relation);
					break;
				case DiagramEvents.NOTE_RELATION_INVALIDATE:
					updateLink(note.getBody() as Relation);
					break;
				case DiagramEvents.NOTE_RELATION_BEGIN:
					beginLink();
					break;
			}
		}
		
	}
}
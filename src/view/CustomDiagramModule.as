package view
{
	import flash.display.MovieClip;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	
	import model.PrAppModel;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	import spark.components.richTextEditorClasses.BoldTool;
	
	import utils.DisplayUtils;
	
	/**
	 * Предок всех модулей. От него следует наследовать новые модули для диаграммы. 
	 * Чтобы подключить новый модуль, отнаследуйся от этого класса, установи необходимый
	 * moduleTool, активирующий модуль.
	 * @author phanatos
	 * 
	 */
	public class CustomDiagramModule extends Mediator
	{
		
		// ссылка на модель приложения
		protected var appModel:PrAppModel;
		// ссылка на медиатор диаграммы
		protected var mdDiagram:MdDiagram;
		// инструмент, активирующий модуль
		protected var moduleTool:int = 0;
		// активен ли данный модуль
		protected var isActive:Boolean = true;

		public function CustomDiagramModule(mediatorName:String=null, viewComponent:Object=null)
		{
			super(mediatorName, viewComponent);
		}
		
		public function get screen():MovieClip
		{
			return viewComponent as MovieClip;
		}
		
		/**
		 * Вызывается при регистрации модуля 
		 * 
		 */
		override public function onRegister():void
		{
			appModel = PrAppModel.getInstance();
			mdDiagram = MdDiagram.getInstance();
			DisplayUtils.clearContainer(screen);
		}
		
		/**
		 * Вызывается при удалении модуля 
		 * 
		 */
		override public function onRemove():void
		{
			appModel = null;
			mdDiagram = null;
		}
		
		/**
		 * Определяет логику поведения курсора при работе модуля. Переопределяется в потомках 
		 * 
		 */
		protected function updateCursor():void
		{
			Mouse.cursor = MouseCursor.AUTO;
		}
		
		/**
		 * Вернет тру, если текущий инструмент диаграммы используется данным модулем 
		 * @return 
		 * 
		 */
		protected function isUsed():Boolean
		{
			return isActive && mdDiagram.curTool == moduleTool;
		}
		
		/**
		 *  Слушаем нотификации 
		 */		
		override public function listNotificationInterests():Array
		{
			
			return[ 
				DiagramEvents.NOTE_TOOLBAR_STATE_CHANGED
			];
		}
		
		/**
		 * Обрабатываем интересующие нас события 
		 * @param note - стандартное послание
		 * 
		 */		
		override public function handleNotification(note:INotification):void
		{
			super.handleNotification(note);
			
			switch (note.getName()) 
			{
				case DiagramEvents.NOTE_TOOLBAR_STATE_CHANGED:
					updateCursor();
					break;
			}
		}
		
	}
}
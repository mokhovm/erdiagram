package main
{
	/**
	 * Режимы отладки приложения и ряд методов для работы с ними 
	 * @author phanatos
	 * 
	 */
	public class DebugModes
	{
		// режимы работы программы
		// локальная отладка
		public static const MODE_DEBUG_LOCAL:int = 1;
		// тестовый сервер
		public static const MODE_DEBUG:int = 2; 
		// боевой сервер
		public static const MODE_RELEASE:int = 4;
		
		// ид тех юзеров, которым разрешена отладка на боевом сервере
		public static const DEBUG_IDS:Array = [0];
		// сетевое ид тех юзеров, которым разрешена отладка на боевом сервере
		public static const DEBUG_NET_IDS:Array = [0];
		
		/**
		 * Если этому игроку разрешена отладка, то выдаст тру 
		 * @param userId - ид юзера
		 * @return 
		 * 
		 */
		public static function canDebug(userId:int):Boolean
		{
			return (DEBUG_IDS.indexOf(userId) != -1);	
		}
		
		/**
		 * Тоже самое, что и canDebug, только по NetId
		 * Это необходимо для консоли
		 * @param netId - ид юзера в соцсети 
		 * @return 
		 * 
		 */
		public static function canDebugByNetId(netId:int, netType:int):Boolean
		{
			return (DEBUG_NET_IDS.indexOf(netId) != -1);	
		}
	}
}